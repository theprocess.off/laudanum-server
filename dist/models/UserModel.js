'use strict';

module.exports = {
  fields: {
    id: 'uuid',
    first_name: 'text',
    last_name: 'text',
    username: 'text',
    password: 'text',
    created: 'timestamp'
  },
  key: ['username']
};