'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _apolloServerExpress = require('apollo-server-express');

var _user = require('../modules/User/user.schema');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Schema = _apolloServerExpress.gql`
  
  type Query{
    """ User Schema """
    User: User!
    Users: [User!]!
  }

  type Mutation {
    Register(username: String!, password: String!): User
  }

  # NOTE: EXPORT HERE ALL THE GRAPH TYPES
  ${_user2.default}

  
`;

exports.default = Schema;