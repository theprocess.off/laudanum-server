'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _user = require('../modules/User/user.resolver');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Resolvers = {
  Query: {
    User: _user2.default.User,
    Users: _user2.default.Users
  },
  Mutation: {
    Register: _user2.default.Register
  }
};

exports.default = Resolvers;