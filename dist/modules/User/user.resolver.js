'use strict';

var _expressCassandra = require('express-cassandra');

var _expressCassandra2 = _interopRequireDefault(_expressCassandra);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {
  // Current User
  User: () => {
    const obj = {
      userID: '1',
      first_name: 'Carlos',
      last_name: 'Arenas',
      username: 'carenas'
    };
    return obj;
  },

  // Get All users function
  Users: async () => {
    await _expressCassandra2.default.instance.User.find({}, (err, users) => {
      if (err) {
        throw err;
      }
      return users;
    });
  },

  // Register Resolver
  Register: async (_, args) => {
    const uuid = _expressCassandra2.default.uuid();
    const user = new _expressCassandra2.default.instance.User({
      id: uuid,
      username: args.username,
      password: args.password,
      created: Date.now()
    });
    await user.save(err => {
      const data = {
        userID: uuid,
        first_name: 'Carlos',
        last_name: 'Arenas',
        username: args.username
      };
      if (err) {
        throw err;
      }
      return data;
    });
  },

  // Login Resolver
  Login: (_, args) => {
    const obj = {
      username: args.username,
      password: args.password
    };
    return obj;
  }
};