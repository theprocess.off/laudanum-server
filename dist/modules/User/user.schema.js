"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
const userSchema = `
  type User {
    " The User ID "
    userID: ID!,
    
    " The User first name "
    first_name: String,

    " The User last name "
    last_name: String,

    " The User username "
    username: String,

    " The User password "
    password: String
  }
`;

exports.default = userSchema;