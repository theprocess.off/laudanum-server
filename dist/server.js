'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _apolloServerExpress = require('apollo-server-express');

var _graphqlTools = require('graphql-tools');

var _authorization = require('./config/authorization');

var _authorization2 = _interopRequireDefault(_authorization);

var _Schema = require('./graphql/Schema');

var _Schema2 = _interopRequireDefault(_Schema);

var _Resolver = require('./graphql/Resolver');

var _Resolver2 = _interopRequireDefault(_Resolver);

var _db = require('./db');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Resolvers
const app = (0, _express2.default)();

// Schemas

const port = process.env.PORT || 8000;

// Parse incoming request
app.use(_bodyParser2.default.json());

// Use the Authorization middleware
app.use(_authorization2.default);

// Connecto
(0, _db2.default)();

/* Import all the fucking typeDefs and resolvers */
const schema = (0, _graphqlTools.makeExecutableSchema)({ typeDefs: _Schema2.default, resolvers: _Resolver2.default });

/* The Graphql middleware endpoint which ... */
const server = new _apolloServerExpress.ApolloServer({
  schema,
  // request the user token
  context: ({ req }) => ({ user: req.headers.authorization })
});
const graphPath = 'graphql';
server.applyMiddleware({ app, graphPath });

/* Just a scammer route hihi */
app.get('/secret', (req, res) => {
  res.send('Welcome to GraphQL Server, if you are not authorized go fuck yourself');
});

app.listen(port, () => console.log(`laudanium listening on port ${port}!`));