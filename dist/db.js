'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _expressCassandra = require('express-cassandra');

var _expressCassandra2 = _interopRequireDefault(_expressCassandra);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// eslint-disable-next-line import/no-mutable-exports
const DBConnector = () => {
  _expressCassandra2.default.setDirectory(`${__dirname}/models`).bind({
    clientOptions: {
      contactPoints: ['172.18.0.2', '127.0.0.1'],
      protocolOptions: { port: 9042 },
      keyspace: 'laudanium',
      queryOptions: { consistency: _expressCassandra2.default.consistencies.one }
    },
    ormOptions: {
      defaultReplicationStrategy: {
        class: 'SimpleStrategy',
        replication_factor: 1
      },
      migration: 'safe'
    }
  });
};

exports.default = DBConnector;