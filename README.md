# laudanum-server

This is the API for laudanum project 
created with express, apollo/graphql and a cassandra ORM :)
thanks to babel we also add the support to class and import/export keywords, enjoy it!

Drink Grog

# PLEASE FOllOW THE NEXT STEPS TO SETUP THE PROJECT AND HACK ON:
- `npm install`
- `npm run start`
- `docker run --name cassandraDb -d -p 7199:7199 -p 7000:7000 -p 9042:9042 -p 9160:9160 -p 7001:7001 cassandra:3.11` // This going to up a cassandra instance on your local env

# TO RUN THE UNIT TEST:
- `npm run test.unit`

# TO RUN THE LINTER:
- `npm run lint`

# TO RUN AS A DOCKER CONTAINER
(ensure to remove all containers)
- `docker-compose stop && docker-compose rm -fv`
- `docker-compose build`
- `docker-compose up && docker-compose logs -f`

# Further work:
 - Init cassandra container before node container
 - Create error handling messages

 