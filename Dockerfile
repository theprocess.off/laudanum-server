FROM node:10.12-alpine

LABEL maintainer="carenas@itexico.net"

EXPOSE 8000
EXPOSE 9042

# Ensure the node_modules/.bin for this project is on system $PATH.
ENV PATH /laudanium/node_modules/.bin:$PATH


# This allows for source changes to take place without having to bust the build cache on deps.
WORKDIR /laudanium
COPY ./package.json package.json
COPY ./package-lock.json package-lock.json
RUN npm install

# Copy over our app's source & build it.
COPY ./src src
COPY ./.babelrc .babelrc
COPY ./.eslintrc .eslintrc
COPY ./tests tests

RUN npm run build

# Use a CMD here (instead of ENTRYPOINT) for easy overwrite in docker ecosystem.
CMD node dist/serve.js