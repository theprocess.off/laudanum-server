import express from 'express';
import dotenv from 'dotenv';
import bodyparser from 'body-parser';
import { ApolloServer } from 'apollo-server-express';
import { makeExecutableSchema } from 'graphql-tools';
// import AuthMiddleware from './config/authorization';

// Schemas
import Schema from './graphql/Schema';

// Resolvers
import Resolvers from './graphql/Resolver';
import DBConnector from './db';

// Load env variables
dotenv.config();

const app = express();
const port = process.env.PORT || 8000;

// Parse incoming request
app.use(bodyparser.json());

// Use the Authorization middleware
// app.use(AuthMiddleware);

// Connecto
DBConnector();

/* Import all the fucking typeDefs and resolvers */
const schema = makeExecutableSchema({ typeDefs: Schema, resolvers: Resolvers });

/* The Graphql middleware endpoint which ... */
const server = new ApolloServer({
  schema,
  // request the user token
  context: ({ req }) => ({ token: req.headers.authorization }),
});
const graphPath = 'graphql';
server.applyMiddleware({ app, graphPath });

/* Just a scammer route hihi */
app.get('/secret', (req, res) => {
  res.send('Welcome to GraphQL Server, if you are not authorized go fuck yourself');
});

app.listen(port, () => console.log(`laudanium listening on port ${port}!`));
