const userSchema = `
  type User {
    " The User ID "
    userID: String!,
    
    " The User first name "
    first_name: String,

    " The User last name "
    last_name: String,

    " The User username "
    username: String,

    " The User password "
    password: String
  }
  
  type Auth{
    " The Token "
    token: String!

    " The User logged "
    username: String
  }
`;

export default userSchema;
