import ExpressCassandra from 'express-cassandra';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

module.exports = {

  /** Get User By ID
   * @param {args.userID}: the user ID
   * @returns {Object} : user
  */
  User: async (_, args) => {
    const user = await ExpressCassandra.instance.User.findOneAsync({ userID: args.userID });
    return user;
  },

  /** Get all Users
   * @param {context.token}: the user token
   * @returns {Array} : users
  */
  Users: async (_, args, context) => {
    const { token } = context;
    let users;
    const verfied = await jwt.verify(token, process.env.JWT_HASH);
    if (verfied.user === 'carenas') {
      users = await ExpressCassandra.instance.User.findAsync({});
      return users;
    }
    return 'Authentication failed';
  },

  // Get the information of current logged user
  Me: async (_, args, context) => {
    const { token } = context;
    const verfied = await jwt.verify(token, process.env.JWT_HASH);
    if (verfied) {
      const me = await ExpressCassandra.instance.User.findOneAsync(
        { username: verfied.username },
      );
      return me;
    }
    return 'Authentication failed';
  },

  // Register and Hash the password
  Register: async (_, args) => {
    const uuid = ExpressCassandra.uuid();
    const passwordHashed = await bcrypt.hash(args.password, 10);
    const user = new ExpressCassandra.instance.User({
      userID: uuid,
      username: args.username,
      password: passwordHashed,
      created: Date.now(),
    });
    try {
      await user.saveAsync();
      return {
        userID: uuid,
        first_name: '',
        last_name: '',
        username: args.username,
      };
    } catch (err) {
      throw err;
    }
  },

  // Login Resolver
  Login: async (_, args) => {
    const user = await ExpressCassandra.instance.User.findOneAsync({ username: args.username });
    const isPasswordCorrect = await bcrypt.compare(args.password, user.password);
    if (isPasswordCorrect) {
      // should return a jwt token
      const token = await jwt.sign({ user: args.username }, process.env.JWT_HASH, { expiresIn: '4h' });
      const response = { username: args.username, token };
      return response;
    }
    throw new Error('Error: invalid user or password');
  },
};
