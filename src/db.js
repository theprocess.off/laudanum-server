import ExpressCassandra from 'express-cassandra';

// eslint-disable-next-line import/no-mutable-exports
const DBConnector = () => {
  ExpressCassandra.setDirectory(`${__dirname}/models`).bind({
    clientOptions: {
      contactPoints: ['172.18.0.2', '127.0.0.1', '192.168.99.100'],
      protocolOptions: { port: 9042 },
      keyspace: 'laudanium',
      queryOptions: { consistency: ExpressCassandra.consistencies.one },
    },
    ormOptions: {
      defaultReplicationStrategy: {
        class: 'SimpleStrategy',
        replication_factor: 1,
      },
      migration: 'safe',
    },
  }, (err) => {
    throw err;
  });
};


export default DBConnector;
