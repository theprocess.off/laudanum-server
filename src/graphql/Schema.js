import { gql } from 'apollo-server-express';
import userSchema from '../modules/User/user.schema';

const Schema = gql`
  
  type Query{
    """ User Schema """
    User(userID: String!): User!
    Users: [User!]!
    Me: User!
  }

  type Mutation {
    Register(username: String!, password: String!): User!
    Login(username: String!, password: String!): Auth!
  }

  # NOTE: EXPORT HERE ALL THE GRAPH TYPES
  ${userSchema}

  
`;

export default Schema;
