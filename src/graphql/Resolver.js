import userResolver from '../modules/User/user.resolver';

const Resolvers = {
  Query: {
    User: userResolver.User,
    Users: userResolver.Users,
  },
  Mutation: {
    Register: userResolver.Register,
    Login: userResolver.Login,
  },
};

export default Resolvers;
